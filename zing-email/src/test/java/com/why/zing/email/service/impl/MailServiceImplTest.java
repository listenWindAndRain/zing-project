package com.why.zing.email.service.impl;

import com.why.zing.email.entity.EmailEntity;
import com.why.zing.email.service.MailService;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailServiceImplTest {

    @Autowired
    private MailService mailService;

    @Test
    public void sendSimpleMail() {
        EmailEntity entity = new EmailEntity();
        String[] tos = new String[1];
        tos[0] = "15732625998@163.com";
        entity.setTo(tos);
        entity.setContent("睁眼看世界测试发送简单邮件！");
        entity.setSubject("睁眼看世界发邮件");
        mailService.sendSimpleMail(entity);
    }

    @Test
    public void sendHtmlMail() throws MessagingException {
        EmailEntity entity = new EmailEntity();
        String[] tos = new String[1];
        tos[0] = "15732625998@163.com";
        entity.setTo(tos);
        entity.setContent("欢迎进入<a href=\"http://www.baidu.com\">百度首页</a>");
        entity.setSubject("睁眼看世界发邮件");
        Map<String, String> attachmentMap = new HashMap<>();
        attachmentMap.put("附件", "E:\\a.txt");
        entity.setAttachmentMap(attachmentMap);
        mailService.sendHtmlMail(entity);
    }

    @Test
    public void sendTemplateMail() throws MessagingException, IOException, TemplateException {
        EmailEntity entity = new EmailEntity();
        String[] tos = new String[1];
        tos[0] = "15732625998@163.com";
        entity.setTo(tos);
        entity.setContent("睁眼看世界测试发送简单邮件！");
        entity.setSubject("睁眼看世界发邮件");

        HashMap<String,String> kvMap = new HashMap<>();
        kvMap.put("username","萌了个兔喵了个咪的");
        entity.setKvMap(kvMap);
        mailService.sendTemplateMail(entity);
    }
}