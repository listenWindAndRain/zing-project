package com.why.zing.email.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Email封装类
 *
 * @author why
 * @date 2019年6月18日
 */
@Data
public class EmailEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 	接收方邮件
	 */
	private String[] to;

	/**
	 * 主题
	 */
	private String subject;

	/**
	 * 邮件内容
	 */
	private String content;

	/**
	 * 模板
	 */
	private String template;


	/**
	 * 附件
	 */
	private Map<String, String> attachmentMap;


	/**
	 * 自定义参数
	 */
	private HashMap<String, String> kvMap;
}
