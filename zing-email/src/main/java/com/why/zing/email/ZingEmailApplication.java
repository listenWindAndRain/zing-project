package com.why.zing.email;

import com.why.zing.email.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZingEmailApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZingEmailApplication.class, args);
    }

}
