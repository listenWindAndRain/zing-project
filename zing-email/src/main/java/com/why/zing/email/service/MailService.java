package com.why.zing.email.service;


import com.why.zing.email.entity.EmailEntity;
import freemarker.template.TemplateException;

import javax.mail.MessagingException;
import java.io.IOException;

public interface MailService {

    /**
     * 发送简单邮件
     * @param emailEntity 邮件内容信息
     */
    void sendSimpleMail(EmailEntity emailEntity);


    /**
     * 发送HTML邮件
     * @param emailEntity 邮件内容信息
     */
    void sendHtmlMail(EmailEntity emailEntity) throws MessagingException;


    /**
     * 发送模板邮件
     * @param emailEntity 邮件内容信息
     */
    void sendTemplateMail(EmailEntity emailEntity) throws MessagingException, IOException, TemplateException;
}
