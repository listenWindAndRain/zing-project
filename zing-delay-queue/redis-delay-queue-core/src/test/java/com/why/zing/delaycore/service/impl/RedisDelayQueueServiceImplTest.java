package com.why.zing.delaycore.service.impl;

import com.why.zing.delaycore.RedisDelayQueueCoreApplication;
import com.why.zing.delaycore.model.Job;
import com.why.zing.delaycore.queue.ReadyQueueContext;
import com.why.zing.delaycore.service.RedisDelayQueueService;
import com.why.zing.delaycore.util.DateUtil;
import lombok.ToString;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RedisDelayQueueCoreApplication.class)
public class RedisDelayQueueServiceImplTest {

    @Autowired
    private RedisDelayQueueService redisDelayQueueService;

    @Autowired
    private ReadyQueueContext readyQueueContext;

    @Test
    public void testReadyQueueContext() throws ExecutionException, InterruptedException {
//        readyQueueContext.runTopicThreads();
        System.out.println("..........");
    }

    @Test
    public void addJob() throws InterruptedException {
        Job job = new Job();
        job.setJobId(UUID.randomUUID().toString());
        job.setTopic("NOTIFY");
        long delay = System.currentTimeMillis() + 10000;
        DateUtil.long2Str(delay);
        job.setDelay(delay);
        job.setBody("{\"amount\":0.01,\"}");
        job.setUrl("https://toc.test.bkjk-inc.com/apollo/cashierbe/testCallback");

        redisDelayQueueService.addJob(job);

        while (true) {
            Thread.sleep(100000000);
        }
    }

    @Test
    public void deleteJob() {
    }
}