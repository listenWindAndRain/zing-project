package com.why.zing.delaycore.controller;

import com.alibaba.fastjson.JSON;
import com.why.zing.common.domain.ZingResult;
import com.why.zing.delaycore.model.Job;
import com.why.zing.delaycore.model.JobDie;
import com.why.zing.delaycore.service.RedisDelayQueueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * job信息管理
 *
 * @author 睁眼看世界
 * @date 2020年1月17日
 */
@Slf4j
@RestController
public class DelayQueueJobController {

    @Autowired
    private RedisDelayQueueService redisDelayQueueService;

    @PostMapping(value = "/addJob")
    public ZingResult addJob(@Validated @RequestBody Job job) {
        log.info("DelayQueueJobController.addJob param is [{}]", JSON.toJSONString(job));
        redisDelayQueueService.addJob(job);
        return ZingResult.success();
    }

    @PostMapping(value = "/deleteJob")
    public ZingResult deleteJob(@Validated @RequestBody JobDie jobDie) {
        log.info("DelayQueueJobController.deleteJob param is [{}]", JSON.toJSONString(jobDie));
        redisDelayQueueService.deleteJob(jobDie);
        return ZingResult.success();
    }
}
