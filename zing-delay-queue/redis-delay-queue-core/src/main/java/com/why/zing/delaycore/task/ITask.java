package com.why.zing.delaycore.task;

/**
 * 任务接口
 *
 * @author 睁眼看世界
 * @date : 2020年1月16日
 */
public interface ITask {
    /**
     * 执行任务
     */
    void doTask();
}
