package com.why.zing.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZingGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZingGatewayApplication.class, args);
    }
}
