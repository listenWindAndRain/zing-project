package com.why.zing.admin.mapper;

import com.why.zing.admin.entity.UserRole;
import tk.mybatis.mapper.common.Mapper;

public interface UserRoleMapper extends Mapper<UserRole> {
}