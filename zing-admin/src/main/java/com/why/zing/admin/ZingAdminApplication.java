package com.why.zing.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZingAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZingAdminApplication.class, args);
    }
}
