package com.why.zing.admin.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "dept")
public class Dept {
    /**
     * ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 上级部门
     */
    private Long pid;

    /**
     * 部门编码
     */
    @Column(name = "dept_code")
    private String deptCode;

    /**
     * 名称
     */
    private String name;

    /**
     * 排序
     */
    @Column(name = "dept_sort")
    private Integer deptSort;

    /**
     * 子部门数目
     */
    @Column(name = "sub_count")
    private Integer subCount;

    /**
     * 状态：1启用、0禁用
     */
    private Boolean enabled;

    /**
     * 创建日期
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取上级部门
     *
     * @return pid - 上级部门
     */
    public Long getPid() {
        return pid;
    }

    /**
     * 设置上级部门
     *
     * @param pid 上级部门
     */
    public void setPid(Long pid) {
        this.pid = pid;
    }

    /**
     * 获取部门编码
     *
     * @return dept_code - 部门编码
     */
    public String getDeptCode() {
        return deptCode;
    }

    /**
     * 设置部门编码
     *
     * @param deptCode 部门编码
     */
    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取排序
     *
     * @return dept_sort - 排序
     */
    public Integer getDeptSort() {
        return deptSort;
    }

    /**
     * 设置排序
     *
     * @param deptSort 排序
     */
    public void setDeptSort(Integer deptSort) {
        this.deptSort = deptSort;
    }

    /**
     * 获取子部门数目
     *
     * @return sub_count - 子部门数目
     */
    public Integer getSubCount() {
        return subCount;
    }

    /**
     * 设置子部门数目
     *
     * @param subCount 子部门数目
     */
    public void setSubCount(Integer subCount) {
        this.subCount = subCount;
    }

    /**
     * 获取状态：1启用、0禁用
     *
     * @return enabled - 状态：1启用、0禁用
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * 设置状态：1启用、0禁用
     *
     * @param enabled 状态：1启用、0禁用
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * 获取创建日期
     *
     * @return create_time - 创建日期
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建日期
     *
     * @param createTime 创建日期
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}