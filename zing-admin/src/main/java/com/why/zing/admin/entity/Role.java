package com.why.zing.admin.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "role")
public class Role {
    /**
     * ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 名称
     */
    @Column(name = "role_name")
    private String roleName;

    /**
     * 角色级别
     */
    @Column(name = "role_level")
    private Integer roleLevel;

    /**
     * 描述
     */
    @Column(name = "role_desc")
    private String roleDesc;

    /**
     * 数据权限
     */
    @Column(name = "data_scope")
    private String dataScope;

    /**
     * 创建日期
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取名称
     *
     * @return role_name - 名称
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * 设置名称
     *
     * @param roleName 名称
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * 获取角色级别
     *
     * @return role_level - 角色级别
     */
    public Integer getRoleLevel() {
        return roleLevel;
    }

    /**
     * 设置角色级别
     *
     * @param roleLevel 角色级别
     */
    public void setRoleLevel(Integer roleLevel) {
        this.roleLevel = roleLevel;
    }

    /**
     * 获取描述
     *
     * @return role_desc - 描述
     */
    public String getRoleDesc() {
        return roleDesc;
    }

    /**
     * 设置描述
     *
     * @param roleDesc 描述
     */
    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }

    /**
     * 获取数据权限
     *
     * @return data_scope - 数据权限
     */
    public String getDataScope() {
        return dataScope;
    }

    /**
     * 设置数据权限
     *
     * @param dataScope 数据权限
     */
    public void setDataScope(String dataScope) {
        this.dataScope = dataScope;
    }

    /**
     * 获取创建日期
     *
     * @return create_time - 创建日期
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建日期
     *
     * @param createTime 创建日期
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}