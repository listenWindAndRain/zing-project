package com.why.zing.admin.mapper;

import com.why.zing.admin.entity.Menu;
import tk.mybatis.mapper.common.Mapper;

public interface MenuMapper extends Mapper<Menu> {
}