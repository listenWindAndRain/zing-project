package com.why.zing.admin.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "job")
public class Job {
    /**
     * ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 岗位名称
     */
    @Column(name = "job_name")
    private String jobName;

    /**
     * 岗位状态
     */
    private Boolean enabled;

    /**
     * 排序
     */
    @Column(name = "job_sort")
    private Integer jobSort;

    /**
     * 创建日期
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取岗位名称
     *
     * @return job_name - 岗位名称
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * 设置岗位名称
     *
     * @param jobName 岗位名称
     */
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    /**
     * 获取岗位状态
     *
     * @return enabled - 岗位状态
     */
    public Boolean getEnabled() {
        return enabled;
    }

    /**
     * 设置岗位状态
     *
     * @param enabled 岗位状态
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * 获取排序
     *
     * @return job_sort - 排序
     */
    public Integer getJobSort() {
        return jobSort;
    }

    /**
     * 设置排序
     *
     * @param jobSort 排序
     */
    public void setJobSort(Integer jobSort) {
        this.jobSort = jobSort;
    }

    /**
     * 获取创建日期
     *
     * @return create_time - 创建日期
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建日期
     *
     * @param createTime 创建日期
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}