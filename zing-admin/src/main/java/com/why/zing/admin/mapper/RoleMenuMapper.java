package com.why.zing.admin.mapper;

import com.why.zing.admin.entity.RoleMenu;
import tk.mybatis.mapper.common.Mapper;

public interface RoleMenuMapper extends Mapper<RoleMenu> {
}