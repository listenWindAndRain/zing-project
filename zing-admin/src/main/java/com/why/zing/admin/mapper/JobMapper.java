package com.why.zing.admin.mapper;

import com.why.zing.admin.entity.Job;
import tk.mybatis.mapper.common.Mapper;

public interface JobMapper extends Mapper<Job> {
}