package com.why.zing.admin.mapper;

import com.why.zing.admin.entity.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {
}