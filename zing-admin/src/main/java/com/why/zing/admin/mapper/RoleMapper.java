package com.why.zing.admin.mapper;

import com.why.zing.admin.entity.Role;
import tk.mybatis.mapper.common.Mapper;

public interface RoleMapper extends Mapper<Role> {
}