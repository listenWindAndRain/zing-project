package com.why.zing.admin.mapper;

import com.why.zing.admin.entity.Dept;
import tk.mybatis.mapper.common.Mapper;

public interface DeptMapper extends Mapper<Dept> {
}