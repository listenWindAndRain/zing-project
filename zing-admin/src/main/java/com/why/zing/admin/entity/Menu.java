package com.why.zing.admin.entity;

import java.util.Date;
import javax.persistence.*;

@Table(name = "menu")
public class Menu {
    /**
     * ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 上级菜单ID
     */
    private Long pid;

    /**
     * 子菜单数目
     */
    @Column(name = "sub_count")
    private Integer subCount;

    /**
     * 菜单类型
     */
    private Integer type;

    /**
     * 菜单标题
     */
    private String title;

    /**
     * 组件名称
     */
    private String name;

    /**
     * 组件
     */
    private String component;

    /**
     * 排序
     */
    @Column(name = "menu_sort")
    private Integer menuSort;

    /**
     * 图标
     */
    private String icon;

    /**
     * 链接地址
     */
    private String path;

    /**
     * 是否外链
     */
    @Column(name = "i_frame")
    private Boolean iFrame;

    /**
     * 隐藏
     */
    private Boolean hidden;

    /**
     * 权限
     */
    private String permission;

    /**
     * 创建日期
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 获取ID
     *
     * @return id - ID
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置ID
     *
     * @param id ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取上级菜单ID
     *
     * @return pid - 上级菜单ID
     */
    public Long getPid() {
        return pid;
    }

    /**
     * 设置上级菜单ID
     *
     * @param pid 上级菜单ID
     */
    public void setPid(Long pid) {
        this.pid = pid;
    }

    /**
     * 获取子菜单数目
     *
     * @return sub_count - 子菜单数目
     */
    public Integer getSubCount() {
        return subCount;
    }

    /**
     * 设置子菜单数目
     *
     * @param subCount 子菜单数目
     */
    public void setSubCount(Integer subCount) {
        this.subCount = subCount;
    }

    /**
     * 获取菜单类型
     *
     * @return type - 菜单类型
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设置菜单类型
     *
     * @param type 菜单类型
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取菜单标题
     *
     * @return title - 菜单标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置菜单标题
     *
     * @param title 菜单标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 获取组件名称
     *
     * @return name - 组件名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置组件名称
     *
     * @param name 组件名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取组件
     *
     * @return component - 组件
     */
    public String getComponent() {
        return component;
    }

    /**
     * 设置组件
     *
     * @param component 组件
     */
    public void setComponent(String component) {
        this.component = component;
    }

    /**
     * 获取排序
     *
     * @return menu_sort - 排序
     */
    public Integer getMenuSort() {
        return menuSort;
    }

    /**
     * 设置排序
     *
     * @param menuSort 排序
     */
    public void setMenuSort(Integer menuSort) {
        this.menuSort = menuSort;
    }

    /**
     * 获取图标
     *
     * @return icon - 图标
     */
    public String getIcon() {
        return icon;
    }

    /**
     * 设置图标
     *
     * @param icon 图标
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * 获取链接地址
     *
     * @return path - 链接地址
     */
    public String getPath() {
        return path;
    }

    /**
     * 设置链接地址
     *
     * @param path 链接地址
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * 获取是否外链
     *
     * @return i_frame - 是否外链
     */
    public Boolean getiFrame() {
        return iFrame;
    }

    /**
     * 设置是否外链
     *
     * @param iFrame 是否外链
     */
    public void setiFrame(Boolean iFrame) {
        this.iFrame = iFrame;
    }

    /**
     * 获取隐藏
     *
     * @return hidden - 隐藏
     */
    public Boolean getHidden() {
        return hidden;
    }

    /**
     * 设置隐藏
     *
     * @param hidden 隐藏
     */
    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    /**
     * 获取权限
     *
     * @return permission - 权限
     */
    public String getPermission() {
        return permission;
    }

    /**
     * 设置权限
     *
     * @param permission 权限
     */
    public void setPermission(String permission) {
        this.permission = permission;
    }

    /**
     * 获取创建日期
     *
     * @return create_time - 创建日期
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建日期
     *
     * @param createTime 创建日期
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取更新时间
     *
     * @return update_time - 更新时间
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * 设置更新时间
     *
     * @param updateTime 更新时间
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}