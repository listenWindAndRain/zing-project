package com.why.zing.admin.mapper;

import com.why.zing.admin.entity.UserJob;
import tk.mybatis.mapper.common.Mapper;

public interface UserJobMapper extends Mapper<UserJob> {
}