package com.why.zing.common.constant;

import java.util.Arrays;

/**
 * 状态转换码
 *
 * @author ：睁眼看世界
 * @date ：2019/8/5 15:46
 */
public enum StatusConvertEnum {

    STATUS_FALSE((byte) 0, false),
    STATUS_TRUE((byte) 1, true);

    private byte code;

    private Boolean info;

    StatusConvertEnum(byte code, boolean info) {
        this.code = code;
        this.info = info;
    }


    public byte getCode() {
        return code;
    }

    public Boolean getInfo() {
        return info;
    }

    public static Boolean getInfoByCode(byte code) {
        return Arrays.stream(values()).filter(e -> e.getCode() == code).map(StatusConvertEnum::getInfo).findFirst().orElse(false);
    }

    public static Byte getCodeByInfo(Boolean info) {
        return Arrays.stream(values()).filter(e -> e.getInfo() && info).map(StatusConvertEnum::getCode).findFirst().orElse((byte) 0);
    }
}
