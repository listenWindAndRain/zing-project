package com.why.zing.common.constant;

/**
 * 数据库中数据的状态
 *
 * @author ：睁眼看世界
 * @date ：2019/8/3 11:19
 */
public enum DataStatusEnum {
    /**
     * 已删除数据
     */
    DELETE((byte) 1),

    /**
     * 未删除数据
     */
    NORMAL((byte) 0);


    private Byte code;

    DataStatusEnum(Byte code) {
        this.code = code;
    }

    public Byte getCode() {
        return code;
    }

}
