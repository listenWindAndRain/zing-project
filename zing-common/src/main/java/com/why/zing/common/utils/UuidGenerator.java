package com.why.zing.common.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 时间戳订单号生成器
 *
 * @author 王洪玉
 * @date 2019/12/12
 */

public class UuidGenerator {

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");

    public static String generatorId() {
        LocalDateTime now = LocalDateTime.now();
        String format = now.format(formatter);
        return format + (ThreadLocalRandom.current().nextInt(99999999) + 1);
    }
}
