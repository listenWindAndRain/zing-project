package com.why.zing.common.exception;

import lombok.Getter;

/**
 * 业务异常父类
 *
 * @author 睁眼看世界
 * @date 2018/11/11
 */

@Getter
public class BusinessException extends ZingException {

    public BusinessException(ExceptionCode exceptionCode) {
        super(exceptionCode);
    }
}
