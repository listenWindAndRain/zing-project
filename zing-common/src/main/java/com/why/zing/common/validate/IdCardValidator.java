package com.why.zing.common.validate;


import com.why.zing.common.utils.ResidentIDCardUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 身份证号校验类
 *
 * @author why
 * @date 2019年4月15日
 */
public class IdCardValidator implements ConstraintValidator<IdCard, String> {

    @Override
    public void initialize(IdCard constraintAnnotation) {

    }

    @Override
    public boolean isValid(String idCard, ConstraintValidatorContext constraintValidatorContext) {
        return ResidentIDCardUtil.isValidateIDCard(idCard);
    }
}
