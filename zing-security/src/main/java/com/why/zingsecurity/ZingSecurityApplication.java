package com.why.zingsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZingSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZingSecurityApplication.class, args);
    }
}
