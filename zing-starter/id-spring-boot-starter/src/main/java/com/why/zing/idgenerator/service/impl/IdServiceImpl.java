package com.why.zing.idgenerator.service.impl;


import com.why.zing.idgenerator.entity.Id;
import com.why.zing.idgenerator.entity.IdType;
import com.why.zing.idgenerator.populater.AtomicIdPopulator;
import com.why.zing.idgenerator.populater.IdPopulator;
import com.why.zing.idgenerator.populater.LockIdPopulator;
import com.why.zing.idgenerator.populater.SyncIdPopulator;
import com.why.zing.idgenerator.utils.CommonUtils;

public class IdServiceImpl extends AbstractIdServiceImpl {

    private static final String SYNC_LOCK_IMPL_KEY = "vesta.sync.lock.impl.key";

    private static final String ATOMIC_IMPL_KEY = "vesta.atomic.impl.key";

    protected IdPopulator idPopulator;

    public IdServiceImpl() {
        super();

        initPopulator();
    }

    public IdServiceImpl(String type) {
        super(type);

        initPopulator();
    }

    public IdServiceImpl(IdType type) {
        super(type);

        initPopulator();
    }

    public void initPopulator() {
        if(idPopulator != null){
        } else if (CommonUtils.isPropKeyOn(SYNC_LOCK_IMPL_KEY)) {
            idPopulator = new SyncIdPopulator();
        } else if (CommonUtils.isPropKeyOn(ATOMIC_IMPL_KEY)) {
            idPopulator = new AtomicIdPopulator();
        } else {
            idPopulator = new LockIdPopulator();
        }
    }

    @Override
    protected void populateId(Id id) {
        idPopulator.populateId(id, this.idMeta);
    }

    public void setIdPopulator(IdPopulator idPopulator) {
        this.idPopulator = idPopulator;
    }
}
