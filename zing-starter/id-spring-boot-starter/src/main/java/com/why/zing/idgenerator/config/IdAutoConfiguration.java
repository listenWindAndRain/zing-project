package com.why.zing.idgenerator.config;

import com.why.zing.idgenerator.factory.IdServiceFactoryBean;
import com.why.zing.idgenerator.service.IdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.why.zing.idgenerator.factory.IdServiceFactoryBean.Type.IP_CONFIGURABLE;

/**
 * ID自动配置类
 *
 * @author 王洪玉
 * @date 2019/12/10
 */

@Configuration
@ConditionalOnClass(IdService.class)
@EnableConfigurationProperties(IdConfig.class)
public class IdAutoConfiguration {

    @Autowired
    private IdConfig idConfig;

    @Bean(name = "idService",initMethod = "init")
    @ConditionalOnMissingBean(IdService.class)
    public IdServiceFactoryBean idServiceFactoryBean() {
        IdServiceFactoryBean idServiceFactoryBean = new IdServiceFactoryBean();
        idServiceFactoryBean.setProviderType(IP_CONFIGURABLE);
        idServiceFactoryBean.setType(idConfig.getType());
        idServiceFactoryBean.setGenMethod(idConfig.getGenMethod());
        idServiceFactoryBean.setMachineId(idConfig.getMachine());
        idServiceFactoryBean.setIps(idConfig.getIps());
        return idServiceFactoryBean;
    }
}
