package com.why.zing.idgenerator.factory;


import com.why.zing.idgenerator.provider.IpConfigurableMachineIdProvider;
import com.why.zing.idgenerator.provider.PropertyMachineIdProvider;
import com.why.zing.idgenerator.service.IdService;
import com.why.zing.idgenerator.service.impl.IdServiceImpl;
import org.springframework.beans.factory.FactoryBean;

public class IdServiceFactoryBean implements FactoryBean<IdService> {

    public enum Type {
        PROPERTY, IP_CONFIGURABLE
    }

    ;

    private Type providerType;

    private long machineId;

    private String ips;

    private long genMethod = -1;
    private long type = -1;
    private long version = -1;

    private IdService idService;

    public void init() {
        if (providerType == null) {
            throw new IllegalArgumentException(
                    "The type of Id service is mandatory.");
        }

        switch (providerType) {
            case PROPERTY:
                idService = constructPropertyIdService(machineId);
                break;
            case IP_CONFIGURABLE:
                idService = constructIpConfigurableIdService(ips);
        }
    }

    @Override
    public IdService getObject() throws Exception {
        return idService;
    }

    private IdService constructPropertyIdService(long machineId) {

        PropertyMachineIdProvider propertyMachineIdProvider = new PropertyMachineIdProvider();
        propertyMachineIdProvider.setMachineId(machineId);

        IdServiceImpl idServiceImpl = new IdServiceImpl();
        idServiceImpl.setMachineIdProvider(propertyMachineIdProvider);
        if (genMethod != -1) {
            idServiceImpl.setGenMethod(genMethod);
        }
        if (type != -1) {
            idServiceImpl.setType(type);
        }
        if (version != -1) {
            idServiceImpl.setVersion(version);
        }
        idServiceImpl.init();

        return idServiceImpl;
    }

    private IdService constructIpConfigurableIdService(String ips) {

        IpConfigurableMachineIdProvider ipConfigurableMachineIdProvider = new IpConfigurableMachineIdProvider(
                ips);

        IdServiceImpl idServiceImpl = new IdServiceImpl();
        idServiceImpl.setMachineIdProvider(ipConfigurableMachineIdProvider);
        if (genMethod != -1)
            idServiceImpl.setGenMethod(genMethod);
        if (type != -1)
            idServiceImpl.setType(type);
        if (version != -1)
            idServiceImpl.setVersion(version);
        idServiceImpl.init();

        return idServiceImpl;
    }


    @Override
    public Class<?> getObjectType() {
        return IdService.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    public Type getProviderType() {
        return providerType;
    }

    public void setProviderType(Type providerType) {
        this.providerType = providerType;
    }

    public long getMachineId() {
        return machineId;
    }

    public void setMachineId(long machineId) {
        this.machineId = machineId;
    }

    public long getGenMethod() {
        return genMethod;
    }

    public void setGenMethod(long genMethod) {
        this.genMethod = genMethod;
    }

    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public String getIps() {
        return ips;
    }

    public void setIps(String ips) {
        this.ips = ips;
    }
}
