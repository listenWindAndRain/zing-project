package com.why.zing.idgenerator.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 生成ID的配置类
 *
 * @author wanghongyu
 * @date 2018/12/7 18:03
 */
@Configuration
@ConfigurationProperties(prefix = "id")
public class IdConfig {


    // 机器号(测试环境指定)
    private Long machine = 0L;


    // 0 表示使用嵌入式发布模式 1 中心 2 REST
    private Long genMethod = 0L;

    // 0 表示最大峰值型  1 最小粒度型
    private Long type = 0L;

    // 服务器IP
    private String ips = "192.168.0.102,192.168.1.216,192.168.0.104";


    public Long getMachine() {
        return machine;
    }

    public void setMachine(Long machine) {
        this.machine = machine;
    }

    public Long getGenMethod() {
        return genMethod;
    }

    public void setGenMethod(Long genMethod) {
        this.genMethod = genMethod;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getIps() {
        return ips;
    }

    public void setIps(String ips) {
        this.ips = ips;
    }
}
