package com.why.zing.idgenerator.populater;


import com.why.zing.idgenerator.entity.Id;
import com.why.zing.idgenerator.entity.IdMeta;

public interface IdPopulator {

    void populateId(Id id, IdMeta idMeta);

}
