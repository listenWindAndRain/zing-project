package com.why.zing.idgenerator.populater;

public interface ResetPopulator {

    void reset();

}
