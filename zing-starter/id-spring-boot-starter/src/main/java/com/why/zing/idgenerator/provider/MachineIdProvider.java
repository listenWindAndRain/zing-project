package com.why.zing.idgenerator.provider;

public interface MachineIdProvider {
    long getMachineId();
}
