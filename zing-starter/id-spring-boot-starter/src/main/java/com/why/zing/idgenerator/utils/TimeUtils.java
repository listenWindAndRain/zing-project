package com.why.zing.idgenerator.utils;


import com.why.zing.idgenerator.entity.IdType;

public class TimeUtils {

    public static final long EPOCH = 1420041600000L;


    public static void validateTimestamp(long lastTimestamp, long timestamp) {
        if (timestamp < lastTimestamp) {
            throw new IllegalStateException(
                    String.format(
                            "Clock moved backwards.  Refusing to generate id for %d second/milisecond.",
                            lastTimestamp - timestamp));
        }
    }

    public static long tillNextTimeUnit(final long lastTimestamp, final IdType idType) {

        long timestamp = TimeUtils.genTime(idType);
        while (timestamp <= lastTimestamp) {
            timestamp = TimeUtils.genTime(idType);
        }


        return timestamp;
    }

    public static long genTime(final IdType idType) {
        if (idType == IdType.MAX_PEAK)
            return (System.currentTimeMillis() - TimeUtils.EPOCH) / 1000;
        else if (idType == IdType.MIN_GRANULARITY)
            return (System.currentTimeMillis() - TimeUtils.EPOCH);

        return (System.currentTimeMillis() - TimeUtils.EPOCH) / 1000;
    }

}
