package com.why.zing.idgenerator.converter;


import com.why.zing.idgenerator.entity.Id;

public interface IdConverter {

    long convert(Id id);

    Id convert(long id);

}
