package com.why.zing.idgenerator.populater;


import com.why.zing.idgenerator.entity.Id;
import com.why.zing.idgenerator.entity.IdMeta;

public class SyncIdPopulator extends BasePopulator {

    public SyncIdPopulator() {
        super();
    }

    @Override
    public synchronized void populateId(Id id, IdMeta idMeta) {
        super.populateId(id, idMeta);
    }

}
