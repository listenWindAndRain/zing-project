package com.why.zing.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZingStorageApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZingStorageApplication.class, args);
    }
}
