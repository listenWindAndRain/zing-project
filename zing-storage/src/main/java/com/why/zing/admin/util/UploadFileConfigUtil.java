package com.why.zing.admin.util;

import com.github.tobato.fastdfs.domain.fdfs.MetaData;
import com.github.tobato.fastdfs.domain.upload.FastImageFile;
import com.why.zing.common.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.InputStream;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * 上传文件属性配置
 *
 * @author 王洪玉
 * @date 2019/12/30
 */
@Slf4j
public class UploadFileConfigUtil {


    /**
     * 只上传文件
     *
     */
    public static FastImageFile crtFastImageFileOnly(InputStream inputStream, Long fileSize, String fileExtName) throws Exception {
        Set<MetaData> metaDataSet = createMetaData();
        return new FastImageFile.Builder()
                .withFile(inputStream, fileSize, fileExtName)
                .withMetaData(metaDataSet)
                .build();
    }

//    /**
//     * 上传文件，按默认方式生成缩略图
//     *
//     * @return
//     * @throws Exception
//     */
//    public static FastImageFile crtFastImageAndCrtThumbImageByDefault() throws Exception {
//        InputStream in = TestUtils.getFileInputStream(TestConstants.PERFORM_FILE_PATH);
//        Set<MetaData> metaDataSet = createMetaData();
//        File file = TestUtils.getFile(TestConstants.PERFORM_FILE_PATH);
//        String fileExtName = FilenameUtils.getExtension(file.getName());
//
//        return new FastImageFile.Builder()
//                .withThumbImage()
//                .withFile(in, file.length(), fileExtName)
//                .withMetaData(metaDataSet)
//                .build();
//    }
//
//
//    /**
//     * 上传文件，按设定尺寸方式生成缩略图
//     *
//     * @return
//     * @throws Exception
//     */
//    public static FastImageFile crtFastImageAndCrtThumbImageBySize() throws Exception {
//        InputStream in = TestUtils.getFileInputStream(TestConstants.PERFORM_FILE_PATH);
//        Set<MetaData> metaDataSet = createMetaData();
//        File file = TestUtils.getFile(TestConstants.PERFORM_FILE_PATH);
//        String fileExtName = FilenameUtils.getExtension(file.getName());
//
//        return new FastImageFile.Builder()
//                .withThumbImage(200, 200)
//                .withFile(in, file.length(), fileExtName)
//                .withMetaData(metaDataSet)
//                .build();
//    }
//
//
//    /**
//     * 上传文件，按比例生成缩略图
//     */
//    public static FastImageFile crtFastImageAndCrtThumbImageByScale() throws Exception {
//        InputStream in = TestUtils.getFileInputStream(TestConstants.PERFORM_FILE_PATH);
//        Set<MetaData> metaDataSet = createMetaData();
//        File file = TestUtils.getFile(TestConstants.PERFORM_FILE_PATH);
//        String fileExtName = FilenameUtils.getExtension(file.getName());
//
//        return new FastImageFile.Builder()
//                .withThumbImage(0.5)  //50%比例
//                .withFile(in, file.length(), fileExtName)
//                .withMetaData(metaDataSet)
//                .build();
//    }


    /**
     * 元数据
     */
    private static Set<MetaData> createMetaData() {
        Set<MetaData> metaDataSet = new HashSet<MetaData>();
        metaDataSet.add(new MetaData("author", "睁眼看世界"));
        metaDataSet.add(new MetaData("createDate", DateUtil.date2StrByPattern(new Date(), DateUtil.YYYY_MM_DD_HH_MM_SS)));
        return metaDataSet;
    }

}
